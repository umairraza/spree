class ChangeExpirydateToProducts < ActiveRecord::Migration
  def change
    change_column :spree_products, :expirydate ,:date
  end
end
